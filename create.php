<?php
require 'db.php';
if (isNotEmpty())
{
    $nama = $_POST['nama'];
    $nis = $_POST['nis'];
    $kelas = $_POST['kelas'];
    $buku = $_POST['buku'];
    $batas = $_POST['batas'];
    $sql = 'INSERT INTO perpustakaan(nis, nama, kelas, buku, batas) VALUES(:nis, :nama, :kelas, :buku, :batas)';
    $statement = $connection->prepare($sql);
    if ($statement->execute([':nis' => $nis, ':nama' => $nama, ':kelas' => $kelas, ':buku' => $buku, ':batas' => $batas]))
    {
        header('Location:data_siswa.php');
    }
    
}

function isNotEmpty()
{
    
    return isset ($_POST['nis']) && isset ($_POST['nama']) && isset ($_POST['kelas']) && isset ($_POST['buku']) && isset ($_POST['batas']);
    
}

?>

<?php require 'header.php'; ?>
<div class="container mt-5">
    <div class="row">
        <div class="col">
            <div class="card mt-5">
                <div class="card-header">
                    <h2>Input Data Siswa</h2>
                </div>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <label>Nis</label>
                            <input maxlength="20" type="number" name="nis" id="nis" class="form-control" placeholder="Input Nis Anda Disini" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Nama</label>
                            <input maxlength="20" type="text" name="nama" id="nama" class="form-control" placeholder="Input Nama Anda Disini" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Kelas</label>
                            <select class="form-control" id="kelas" name="kelas" required>
                                <option></option>
                                <option value="XII IPA">XII IPA</option>
                                <option value="XI RPL">XI RPL</option>
                                <option value="X TKJ">X TKJ</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Judul Buku Yang Dipinjam</label>
                            <input maxlength="20" type="text" name="buku" id="buku" class="form-control" placeholder="Input Disini" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Batas Waktu Peminjaman Buku</label>
                            <select class="form-control" id="batas" name="batas" required>
                                <option></option>
                                <option value="3 Hari">3 Hari</option>
                                <option value="6 Hari">6 Hari</option>
                                <option value="9 Hari">9 Hari</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </div>
</div>
<?php require 'footer.php'; ?>